package device.protocol;

public interface GeneralProtocol {

	public String getTipPacket();

	public Long getNumImei();
	
	public String getDataHora();
	
	public Double getLatitude();
	
	public Double getLongitude();

	public String getTipLatitude();
	
	public String getTipLongitude();	
	
	public Integer getVelocidade();
	
	public Integer getDirecao();
	
}
