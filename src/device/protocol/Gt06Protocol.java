package device.protocol;

public interface Gt06Protocol {

	public String getBitInicial();

	public String getTamanhoPacote();

	public Integer getNumSerial();
	
	public String getNumSerialHex();

	public String getErrorCheck();

	public String getBitFinal();
	
	public byte[] getMsgResponseLogin();
	
	public byte[] getMsgResponseStatus();
	
	public Integer getQtdSatelites();
	
	public String getStatusDirecaoBinario();
	
	public Integer getIndTempoReal();
	
	public Integer getIndGpsPosicionado();
	
	public Integer getMcc();
	
	public Integer getMnc();
	
	public Integer getLac();
	
	public Integer getTorreCelular();
}
