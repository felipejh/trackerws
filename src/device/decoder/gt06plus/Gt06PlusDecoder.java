package device.decoder.gt06plus;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.regex.Pattern;

import device.protocol.GeneralProtocol;
import util.Converter;

public class Gt06PlusDecoder implements GeneralProtocol {

	private String packet; // '*HQ,865205034716521,V1,174604,A,2909.54122,S,05112.80415,W,0.00,89,140919,E7E7FBFF#
	private String dataPacket[];
	private String numImei;
	private String tipPacket;
	private String dthLocation;
	private String numLatitude;
	private String tipLatitude;
	private String numLongitude;
	private String tipLongitude;
	private String speed;
	private String direction;
	private String dtaLocation;

	/**
	 * 
	 * Recebe o pacote enviado pelo rastreador e chama método splitPacket para
	 * desmembrar a String.
	 * 
	 * @param packet Pacote com a mensagem enviada pelo rastreador para o servidor
	 */
	public Gt06PlusDecoder(String packet) {
		this.packet = packet;

		this.splitPacket();
	}

	/**
	 * Desmembra a string separada por vírgulas, armazena no array e atribui a cada
	 * variável
	 * 
	 */
	private void splitPacket() {
		this.dataPacket = this.packet.split(Pattern.quote(","));

		this.numImei = dataPacket[1];
		this.tipPacket = dataPacket[2];

		if (Gt06PlusTipsPacket.TIP_PACKET_NORMAL_V1.getTipPacket().equals(this.tipPacket)) {
			this.dthLocation = dataPacket[3];
			this.numLatitude = dataPacket[5];
			this.tipLatitude = dataPacket[6];
			this.numLongitude = dataPacket[7];
			this.tipLongitude = dataPacket[8];
			this.speed = dataPacket[9];
			this.direction = dataPacket[10];
			this.dtaLocation = dataPacket[11];
		}

		if (Gt06PlusTipsPacket.TIP_PACKET_HEARTBEAT_HTBT.getTipPacket().equals(this.tipPacket.replace("#", ""))) {

		}
	}

	@Override
	public Long getNumImei() {
		return Converter.toLong(this.numImei);
	}

	@Override
	public String getDataHora() {
		String date = this.dtaLocation;
		String time = this.dthLocation; // 120819

		StringBuilder dateTime = new StringBuilder();
		dateTime.append("20");
		dateTime.append(date.substring(4, 6));
		dateTime.append("-");
		dateTime.append(date.substring(2, 4));
		dateTime.append("-");
		dateTime.append(date.substring(0, 2));
		dateTime.append(" ");
		dateTime.append(time.substring(0, 2));
		dateTime.append(":");
		dateTime.append(time.substring(2, 4));
		dateTime.append(":");
		dateTime.append(time.substring(4, 6));

		return dateTime.toString();
	}

	@Override
	public Integer getVelocidade() {
		String speedRaw = this.speed;

		Double kmh = Converter.toDouble(speedRaw) * 1.852;

		return Converter.toInteger(kmh);
	}

	@Override
	public String getTipPacket() {
		return this.tipPacket;
	}

	@Override
	public Double getLatitude() {
		String latitudeRaw = this.numLatitude;

		String degrees = latitudeRaw.substring(0, 2);
		String minutes = Converter.toString(Converter.toDouble(latitudeRaw.substring(2, 10).replace(".", "")) / 60);

		BigDecimal bdLatitude = new BigDecimal(minutes).setScale(0, RoundingMode.HALF_EVEN);

		minutes = Converter.toString(bdLatitude.doubleValue()).replace(".", "");

		latitudeRaw = degrees + "." + minutes;
		Double latitude = Converter.toDouble(latitudeRaw);

		if ("S".equals(this.getTipLatitude())) {
			latitude = latitude * (-1);
		}

		return latitude;
	}

	@Override
	public Double getLongitude() {
		String longitudeRaw = this.numLongitude;

		String degrees = longitudeRaw.substring(0, 3);
		String minutes = Converter.toString(Converter.toDouble(longitudeRaw.substring(3, 11).replace(".", "")) / 60);

		BigDecimal bdLongitude = new BigDecimal(minutes).setScale(0, RoundingMode.HALF_EVEN);

		minutes = Converter.toString(bdLongitude.doubleValue()).replace(".", "");

		longitudeRaw = degrees + "." + minutes;
		Double longitude = Converter.toDouble(longitudeRaw);

		if ("W".equals(this.getTipLongitude())) {
			longitude = longitude * (-1);
		}

		return longitude;
	}

	@Override
	public String getTipLatitude() {
		return this.tipLatitude;
	}

	@Override
	public String getTipLongitude() {
		return this.tipLongitude;
	}

	@Override
	public Integer getDirecao() {
		return Converter.toInteger(this.direction);
	}

}
