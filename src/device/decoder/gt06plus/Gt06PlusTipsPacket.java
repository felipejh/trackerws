package device.decoder.gt06plus;

public enum Gt06PlusTipsPacket {

	TIP_PACKET_NORMAL_V1("V1"), TIP_PACKET_HEARTBEAT_HTBT("HTBT");

	private String tipPacket;

	Gt06PlusTipsPacket(String tipPacket) {
		this.tipPacket = tipPacket;
	}

	public String getTipPacket() {
		return tipPacket;
	}

}
