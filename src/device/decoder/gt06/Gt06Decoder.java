package device.decoder.gt06;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;

import device.protocol.GeneralProtocol;
import device.protocol.Gt06Protocol;
import util.Converter;
import util.NumberConstants;

public class Gt06Decoder implements Gt06Protocol, GeneralProtocol, NumberConstants {

	private static final String TIP_LOGIN = "01";
	private static final String TIP_LOCALIZACAO = "12";
	private static final String TIP_STATUS = "13";
	private static final String TIP_STRING = "15";
	private static final String TIP_ALERTA = "16";
	private static final String TIP_FONE = "1A";
	private static final String TIP_ENVIO = "80";

	private static final String TIP_LONGITUDE_1_LESTE = "L"; // 1
	private static final String TIP_LONGITUDE_2_OESTE = "O"; // 2
	private static final String TIP_LATITUDE_1_SUL = "S";//1
	private static final String TIP_LATITUDE_2_NORTE = "N";

	private String packet;

	public Gt06Decoder(String packet) {
		this.packet = packet;
	}

	public String getPacket() {
		return this.packet;
	}

	public void setPacket(String packet) {
		this.packet = packet;
	}

	@Override
	public String getBitInicial() {
		return this.packet.substring(0, 4);
	}

	@Override
	public String getTamanhoPacote() {
		String hex = this.packet.substring(4, 6);
		Integer qtdBytes = Integer.valueOf(hex, 16);

		return String.valueOf(qtdBytes);
	}

	@Override
	public String getTipPacket() {
		String hex = this.packet.substring(6, 8);

		if (TIP_LOGIN.equals(hex)) {
			return TIP_LOGIN;
		}
		if (TIP_LOCALIZACAO.equals(hex)) {
			return TIP_LOCALIZACAO;
		}
		if (TIP_STATUS.equals(hex)) {
			return TIP_STATUS;
		}
		if (TIP_STRING.equals(hex)) {
			return TIP_STRING;
		}
		if (TIP_ALERTA.equals(hex)) {
			return TIP_ALERTA;
		}
		if (TIP_FONE.equals(hex)) {
			return TIP_FONE;
		}
		if (TIP_ENVIO.equals(hex)) {
			return TIP_ENVIO;
		}
		return null;
	}

	@Override
	public Long getNumImei() {

		if (TIP_LOGIN.equals(this.getTipPacket())) {
			String hex = this.packet.substring(8, 24);

			if ("0".equals(hex.substring(0, 1))) {
				return Converter.toLong(hex.substring(1, 16));
			} else {
				return Converter.toLong(hex);
			}
		}
		return null;
	}

	@Override
	public Integer getNumSerial() {
		if (TIP_LOGIN.equals(this.getTipPacket())) {
			return Integer.valueOf(this.packet.substring(24, 28), 16);
		}

		if (TIP_LOCALIZACAO.equals(this.getTipPacket())) {
			return Integer.valueOf(this.packet.substring(60, 64), 16);
		}
		return null;
	}

	@Override
	public String getNumSerialHex() {

		if (TIP_LOGIN.equals(this.getTipPacket())) {
			return this.packet.substring(24, 28);
		}

		if (TIP_STATUS.equals(this.getTipPacket())) {
			return this.packet.substring(18, 22);
		}

		return null;
	}

	@Override
	public String getErrorCheck() {

		if (TIP_LOGIN.equals(this.getTipPacket())) {
			return this.packet.substring(28, 32);
		}

		if (TIP_STATUS.equals(this.getTipPacket())) {
			return this.packet.substring(22, 26);
		}

		return null;
	}

	@Override
	public String getBitFinal() {

		if (TIP_LOGIN.equals(this.getTipPacket())) {
			return this.packet.substring(32, 36);
		}

		if (TIP_STATUS.equals(this.getTipPacket())) {
			return this.packet.substring(26, 30);
		}

		return null;
	}

	@Override
	public String getDataHora() {
		if (TIP_LOCALIZACAO.equals(this.getTipPacket())) {
			return this.getHoraDataFormat(this.packet.substring(8, 20));
		}
		return null;
	}

	@Override
	public byte[] getMsgResponseLogin() {
		return null;
	}

	private String getHoraDataFormat(String dataHora) {

		String ano = dataHora.substring(0, 2);
		String mes = dataHora.substring(2, 4);
		String dia = dataHora.substring(4, 6);
		String hora = dataHora.substring(6, 8);
		String minuto = dataHora.substring(8, 10);
		String segundo = dataHora.substring(10, 12);

		StringBuilder sb = new StringBuilder();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		sb.append(String.valueOf(sdf.format(new Date())).substring(0, 2));
		sb.append(String.valueOf(Integer.valueOf(ano, 16)));

		sb.append("-");
		sb.append(String.valueOf(Integer.valueOf(mes, 16)));
		sb.append("-");
		sb.append(String.valueOf(Integer.valueOf(dia, 16)));
		sb.append(" ");
		sb.append(String.valueOf(Integer.valueOf(hora, 16)));
		sb.append(":");
		sb.append(String.valueOf(Integer.valueOf(minuto, 16)));
		sb.append(":");
		sb.append(String.valueOf(Integer.valueOf(segundo, 16)));

		return sb.toString();
	}

	@Override
	public Double getLatitude() {

		String latitudeString = this.packet.substring(22, 30);
		Double latitude = 0.0;

		if (TIP_LATITUDE_1_SUL.equals(this.getTipLatitude())) {
			latitude = (Double.valueOf(Integer.valueOf(latitudeString, 16)) / 1800000) * -1;
		}

		if (TIP_LATITUDE_2_NORTE.equals(this.getTipLatitude())) {
			latitude = Double.valueOf(Integer.valueOf(latitudeString, 16)) / 1800000;
		}

		BigDecimal bdLatitude = new BigDecimal(latitude).setScale(6, RoundingMode.HALF_EVEN);

		return bdLatitude.doubleValue();

	}

	@Override
	public Double getLongitude() {

		String longitudeString = this.packet.substring(30, 38);
		Double longitude = 0.0;

		if (TIP_LONGITUDE_1_LESTE.equals(this.getTipLongitude())) {
			longitude = Double.valueOf(Integer.valueOf(longitudeString, 16)) / 1800000;
		}

		if (TIP_LONGITUDE_2_OESTE.equals(this.getTipLongitude())) {
			longitude = (Double.valueOf(Integer.valueOf(longitudeString, 16)) / 1800000) * -1;
		}

		BigDecimal bdLongitude = new BigDecimal(longitude).setScale(6, RoundingMode.HALF_EVEN);

		return bdLongitude.doubleValue();

	}

	@Override
	public Integer getQtdSatelites() {

		String informacaoSat = this.packet.substring(20, 22);

		return Integer.valueOf(informacaoSat.substring(1), 16);
	}

	@Override
	public Integer getVelocidade() {

		String velocidade = this.packet.substring(38, 40);

		return Integer.valueOf(velocidade, 16);
	}

	@Override
	public Integer getDirecao() {

		String direcaoBinario = this.getStatusDirecaoBinario().substring(6, 16);

		return Integer.parseInt(direcaoBinario, 2);
	}

	@Override
	public String getStatusDirecaoBinario() {

		String statusDirecao = this.packet.substring(40, 44);

		Integer statusDirecaoDecimal = Integer.valueOf(statusDirecao, 16);
		return "00" + Integer.toBinaryString(statusDirecaoDecimal);

	}

	@Override
	public Integer getIndTempoReal() {

		return Integer.valueOf(this.getStatusDirecaoBinario().substring(2, 3));
	}

	@Override
	public Integer getIndGpsPosicionado() {

		return Integer.valueOf(this.getStatusDirecaoBinario().substring(3, 4));
	}

	@Override
	public String getTipLongitude() {

		Integer tipLongitudeRaw = Integer.valueOf(this.getStatusDirecaoBinario().substring(4, 5));
		tipLongitudeRaw = +1;

		String tipLongitude = null;
		if (INT_1.equals(tipLongitudeRaw)) {
			tipLongitude = TIP_LONGITUDE_1_LESTE;
		}
		if (INT_2.equals(tipLongitudeRaw)) {
			tipLongitude = TIP_LONGITUDE_2_OESTE;
		}

		return tipLongitude;

	}

	@Override
	public String getTipLatitude() {

		Integer tipLatitudeRaw = Integer.valueOf(this.getStatusDirecaoBinario().substring(5, 6));
		tipLatitudeRaw = +1;

		String tipLatitude = null;
		if (INT_1.equals(tipLatitudeRaw)) {
			tipLatitude = TIP_LATITUDE_1_SUL;
		}

		if (INT_2.equals(tipLatitudeRaw)) {
			tipLatitude = TIP_LATITUDE_2_NORTE;
		}

		return tipLatitude;
	}

	@Override
	public Integer getMcc() {

		return Integer.valueOf(this.packet.substring(44, 48), 16);
	}

	@Override
	public Integer getMnc() {

		return Integer.valueOf(this.packet.substring(48, 50), 16);
	}

	@Override
	public Integer getLac() {
		return Integer.valueOf(this.packet.substring(50, 54), 16);
	}

	@Override
	public Integer getTorreCelular() {
		return Integer.valueOf(this.packet.substring(54, 60), 16);
	}

	@Override
	public byte[] getMsgResponseStatus() {
		return null;
	}

}
