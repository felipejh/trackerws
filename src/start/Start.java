package start;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import ejb.LogsRastreadorEJB;
import listener.Gt06Listener;
import listener.Gt06PlusListener;

@Singleton
@Startup
public class Start {

	@Inject
	private LogsRastreadorEJB logsRastreadorEJB;
	
	@PostConstruct
	public void start() {
		
		new Gt06Listener().startServer(logsRastreadorEJB);
		new Gt06PlusListener().startServer(logsRastreadorEJB);

	}

}
