package util;

import java.math.BigDecimal;

public interface NumberConstants {

	public static final Integer INT_0 = 0;
	public static final Integer INT_1 = 1;
	public static final Integer INT_2 = 2;

	public static final Long LONG_0 = 0L;
	public static final Long LONG_1 = 1L;
	public static final Long LONG_2 = 2L;

	public static final BigDecimal BIG_0 = BigDecimal.ZERO;
	public static final BigDecimal BIG_1 = BigDecimal.ONE;
	public static final BigDecimal BIG_10 = BigDecimal.TEN;
	public static final BigDecimal BIG_100 = new BigDecimal(100);
}
