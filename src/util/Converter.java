package util;

import java.math.BigDecimal;

import javax.faces.convert.ConverterException;

public class Converter {

	public static String byteArrayToHex(byte[] a) {
		StringBuilder sb = new StringBuilder(a.length * 2);
		for (byte b : a)
			sb.append(String.format("%02x", b));
		return sb.toString();
	}

	public static BigDecimal toBigDecimal(Object source) throws ConverterException {
		if (source == null) {
			return null;
		}
		if (source instanceof BigDecimal) {
			return (BigDecimal) source;
		} else if (source instanceof Integer) {
			return new BigDecimal((Integer) source);
		} else if (source instanceof Long) {
			return new BigDecimal((Long) source);
		} else if (source instanceof Double) {
			return new BigDecimal((Double) source);
		}
		try {
			String val = toString(source);
			val = val.replace((char) 160, '*');// Retirar os espaços do formato frances
			val = val.replaceAll("[\\., ]", "*");
			int dec = val.lastIndexOf("*");
			if (dec >= 0) {
				val = val.substring(0, dec) + "." + val.substring(dec + 1);
			}
			val = val.replace("*", "");
			return new BigDecimal(val);
		} catch (NumberFormatException e) {
			throw new ConverterException(e);
		}
	}

	public static String toString(Object source) {
		if (source == null) {
			return null;
		}

		return String.valueOf(source);
	}

	public static Integer toInteger(String source) throws ConverterException {
		if (source == null) {
			return null;
		}
		try {
			return toBigDecimal(source).intValue();
		} catch (NumberFormatException e) {
			throw new ConverterException(e);
		}
	}

	public static Integer toInteger(Object source) throws ConverterException {
		if (source == null) {
			return null;
		}
		if (source instanceof Number) {
			Number num = (Number) source;
			return num.intValue();
		}
		return toInteger(toString(source));
	}

	public static Double toDouble(String source) throws ConverterException {
		if (source == null) {
			return null;
		}
		try {
			return Double.parseDouble(source.replace(",", "."));
		} catch (NumberFormatException e) {
			throw new ConverterException(e);
		}
	}
	
	/**
	 * Converte um String em Long
	 * 
	 * @param source
	 * @return
	 * @throws ConverterException
	 */
	public static Long toLong(String source) throws ConverterException {
		if (source == null) {
			return null;
		}
		try {
			return toBigDecimal(source).longValue();
		} catch (NumberFormatException e) {
			throw new ConverterException(e);
		}
	}

	/**
	 * Conveter um BigDecimal em Long
	 * 
	 * @param source
	 * @return
	 */
	public static Long toLong(BigDecimal source) throws ConverterException {
		if (source == null) {
			return null;
		}
		try {
			return source.longValue();
		} catch (NumberFormatException e) {
			throw new ConverterException(e);
		}
	}

	public static Long toLong(Object source) throws ConverterException {
		if (source == null) {
			return null;
		}
		if (source instanceof Number) {
			Number num = (Number) source;
			return num.longValue();
		}
		return toLong(toString(source));
	}

}
