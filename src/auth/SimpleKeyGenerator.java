package auth;

import java.security.Key;

import javax.crypto.spec.SecretKeySpec;

public class SimpleKeyGenerator implements KeyGenerator {
	@Override
	public Key generateKey() {
		String keyString = "c8372e8c56a945b7b5dab3dc5eb08090-bfc008f5d22d4ca1a9a3261caec120ee-b9d9c4b467624be8ad624e34f7504827-53b87210d7f84345bc7d9d62813da33f";
		Key key = new SecretKeySpec(keyString.getBytes(), 0, keyString.getBytes().length, "HmacSHA512");
		return key;
	}

}
