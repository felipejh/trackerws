package exception;

public class AuthInvalidaExpiradaException extends Exception {
	private static final long serialVersionUID = -4437900100098478832L;

	public AuthInvalidaExpiradaException() {
		super();
	}
}
