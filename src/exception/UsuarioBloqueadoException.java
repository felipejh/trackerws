package exception;

public class UsuarioBloqueadoException extends Exception {
	private static final long serialVersionUID = 7006257525922008298L;

	public UsuarioBloqueadoException() {
		super();
	}
}
