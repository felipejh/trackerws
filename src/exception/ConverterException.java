package exception;

public class ConverterException extends RuntimeException {

	private static final long serialVersionUID = -6305158379757765347L;

	public ConverterException(Throwable e) {
		super(e);
	}
}
