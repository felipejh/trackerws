package entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LOG_ENTRADAS")
public class LogEntradas {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Integer id;

	@Column(name = "IP")
	private String ip;

	@Column(name = "IMEI")
	private Long imei;

	@Column(name = "LATITUDE")
	private Double latitude;

	@Column(name = "LONGITUDE")
	private Double longitude;

	@Column(name = "QTD_SATELITES")
	private Integer qtdSatelites;

	@Column(name = "DTH_LOCALIZACAO")
	private Date dthLocalização;

	@Column(name = "VELOCIDADE")
	private Integer velocidade;

	@Column(name = "DIRECAO")
	private Integer direcao;

	@Column(name = "MCC")
	private Integer mcc;

	@Column(name = "MNC")
	private Integer mnc;

	@Column(name = "LAC")
	private Integer lac;

	@Column(name = "TORRE_CELULAR")
	private Integer torreCelular;

	@Column(name = "NUM_SEQ_SERIAL")
	private Integer numSeqSerial;

	@Column(name = "IND_TEMPO_REAL")
	private Integer indTempoReal;

	@Column(name = "IND_GPS_POSICIONADO")
	private Integer indGpsPosicionado;

	@Column(name = "DTH_CADASTRO")
	private Date dthCadastro;

	@Column(name = "DTH_ATUALIZACAO")
	private Date dthAtualizacao;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Long getImei() {
		return imei;
	}

	public void setImei(Long imei) {
		this.imei = imei;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Integer getQtdSatelites() {
		return qtdSatelites;
	}

	public void setQtdSatelites(Integer qtdSatelites) {
		this.qtdSatelites = qtdSatelites;
	}

	public Date getDthLocalização() {
		return dthLocalização;
	}

	public void setDthLocalização(Date dthLocalização) {
		this.dthLocalização = dthLocalização;
	}

	public Integer getVelocidade() {
		return velocidade;
	}

	public void setVelocidade(Integer velocidade) {
		this.velocidade = velocidade;
	}

	public Integer getDirecao() {
		return direcao;
	}

	public void setDirecao(Integer direcao) {
		this.direcao = direcao;
	}

	public Integer getMcc() {
		return mcc;
	}

	public void setMcc(Integer mcc) {
		this.mcc = mcc;
	}

	public Integer getMnc() {
		return mnc;
	}

	public void setMnc(Integer mnc) {
		this.mnc = mnc;
	}

	public Integer getLac() {
		return lac;
	}

	public void setLac(Integer lac) {
		this.lac = lac;
	}

	public Integer getTorreCelular() {
		return torreCelular;
	}

	public void setTorreCelular(Integer torreCelular) {
		this.torreCelular = torreCelular;
	}

	public Integer getNumSeqSerial() {
		return numSeqSerial;
	}

	public void setNumSeqSerial(Integer numSeqSerial) {
		this.numSeqSerial = numSeqSerial;
	}

	public Integer getIndTempoReal() {
		return indTempoReal;
	}

	public void setIndTempoReal(Integer indTempoReal) {
		this.indTempoReal = indTempoReal;
	}

	public Integer getIndGpsPosicionado() {
		return indGpsPosicionado;
	}

	public void setIndGpsPosicionado(Integer indGpsPosicionado) {
		this.indGpsPosicionado = indGpsPosicionado;
	}

	public Date getDthCadastro() {
		return dthCadastro;
	}

	public void setDthCadastro(Date dthCadastro) {
		this.dthCadastro = dthCadastro;
	}

	public Date getDthAtualizacao() {
		return dthAtualizacao;
	}

	public void setDthAtualizacao(Date dthAtualizacao) {
		this.dthAtualizacao = dthAtualizacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((direcao == null) ? 0 : direcao.hashCode());
		result = prime * result + ((dthAtualizacao == null) ? 0 : dthAtualizacao.hashCode());
		result = prime * result + ((dthCadastro == null) ? 0 : dthCadastro.hashCode());
		result = prime * result + ((dthLocalização == null) ? 0 : dthLocalização.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((imei == null) ? 0 : imei.hashCode());
		result = prime * result + ((indGpsPosicionado == null) ? 0 : indGpsPosicionado.hashCode());
		result = prime * result + ((indTempoReal == null) ? 0 : indTempoReal.hashCode());
		result = prime * result + ((ip == null) ? 0 : ip.hashCode());
		result = prime * result + ((lac == null) ? 0 : lac.hashCode());
		result = prime * result + ((latitude == null) ? 0 : latitude.hashCode());
		result = prime * result + ((longitude == null) ? 0 : longitude.hashCode());
		result = prime * result + ((mcc == null) ? 0 : mcc.hashCode());
		result = prime * result + ((mnc == null) ? 0 : mnc.hashCode());
		result = prime * result + ((numSeqSerial == null) ? 0 : numSeqSerial.hashCode());
		result = prime * result + ((qtdSatelites == null) ? 0 : qtdSatelites.hashCode());
		result = prime * result + ((torreCelular == null) ? 0 : torreCelular.hashCode());
		result = prime * result + ((velocidade == null) ? 0 : velocidade.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LogEntradas other = (LogEntradas) obj;
		if (direcao == null) {
			if (other.direcao != null)
				return false;
		} else if (!direcao.equals(other.direcao))
			return false;
		if (dthAtualizacao == null) {
			if (other.dthAtualizacao != null)
				return false;
		} else if (!dthAtualizacao.equals(other.dthAtualizacao))
			return false;
		if (dthCadastro == null) {
			if (other.dthCadastro != null)
				return false;
		} else if (!dthCadastro.equals(other.dthCadastro))
			return false;
		if (dthLocalização == null) {
			if (other.dthLocalização != null)
				return false;
		} else if (!dthLocalização.equals(other.dthLocalização))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (imei == null) {
			if (other.imei != null)
				return false;
		} else if (!imei.equals(other.imei))
			return false;
		if (indGpsPosicionado == null) {
			if (other.indGpsPosicionado != null)
				return false;
		} else if (!indGpsPosicionado.equals(other.indGpsPosicionado))
			return false;
		if (indTempoReal == null) {
			if (other.indTempoReal != null)
				return false;
		} else if (!indTempoReal.equals(other.indTempoReal))
			return false;
		if (ip == null) {
			if (other.ip != null)
				return false;
		} else if (!ip.equals(other.ip))
			return false;
		if (lac == null) {
			if (other.lac != null)
				return false;
		} else if (!lac.equals(other.lac))
			return false;
		if (latitude == null) {
			if (other.latitude != null)
				return false;
		} else if (!latitude.equals(other.latitude))
			return false;
		if (longitude == null) {
			if (other.longitude != null)
				return false;
		} else if (!longitude.equals(other.longitude))
			return false;
		if (mcc == null) {
			if (other.mcc != null)
				return false;
		} else if (!mcc.equals(other.mcc))
			return false;
		if (mnc == null) {
			if (other.mnc != null)
				return false;
		} else if (!mnc.equals(other.mnc))
			return false;
		if (numSeqSerial == null) {
			if (other.numSeqSerial != null)
				return false;
		} else if (!numSeqSerial.equals(other.numSeqSerial))
			return false;
		if (qtdSatelites == null) {
			if (other.qtdSatelites != null)
				return false;
		} else if (!qtdSatelites.equals(other.qtdSatelites))
			return false;
		if (torreCelular == null) {
			if (other.torreCelular != null)
				return false;
		} else if (!torreCelular.equals(other.torreCelular))
			return false;
		if (velocidade == null) {
			if (other.velocidade != null)
				return false;
		} else if (!velocidade.equals(other.velocidade))
			return false;
		return true;
	}

}
