package entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DISPOSITIVOS")
public class Dispositivos {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Integer id;

	@Column(name = "FUNCIONALIDADE_ID")
	private Integer funcionalidadeId;

	@Column(name = "MODELO_ID")
	private Integer modeloId;

	@Column(name = "IMEI")
	private Long imei;

	@Column(name = "NUMERO_GSM")
	private Long numeroGsm;

	@Column(name = "DTH_CADASTRO")
	private Date dthCadastro;

	@Column(name = "DTH_ATUALIZACAO")
	private Date dthAtualizacao;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getFuncionalidadeId() {
		return funcionalidadeId;
	}

	public void setFuncionalidadeId(Integer funcionalidadeId) {
		this.funcionalidadeId = funcionalidadeId;
	}

	public Integer getModeloId() {
		return modeloId;
	}

	public void setModeloId(Integer modeloId) {
		this.modeloId = modeloId;
	}

	public Long getImei() {
		return imei;
	}

	public void setImei(Long imei) {
		this.imei = imei;
	}

	public Long getNumeroGsm() {
		return numeroGsm;
	}

	public void setNumeroGsm(Long numeroGsm) {
		this.numeroGsm = numeroGsm;
	}

	public Date getDthCadastro() {
		return dthCadastro;
	}

	public void setDthCadastro(Date dthCadastro) {
		this.dthCadastro = dthCadastro;
	}

	public Date getDth_atualizacao() {
		return dthAtualizacao;
	}

	public void setDthAtualizacao(Date dthAtualizacao) {
		this.dthAtualizacao = dthAtualizacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dthAtualizacao == null) ? 0 : dthAtualizacao.hashCode());
		result = prime * result + ((dthCadastro == null) ? 0 : dthCadastro.hashCode());
		result = prime * result + ((funcionalidadeId == null) ? 0 : funcionalidadeId.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((imei == null) ? 0 : imei.hashCode());
		result = prime * result + ((modeloId == null) ? 0 : modeloId.hashCode());
		result = prime * result + ((numeroGsm == null) ? 0 : numeroGsm.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dispositivos other = (Dispositivos) obj;
		if (dthAtualizacao == null) {
			if (other.dthAtualizacao != null)
				return false;
		} else if (!dthAtualizacao.equals(other.dthAtualizacao))
			return false;
		if (dthCadastro == null) {
			if (other.dthCadastro != null)
				return false;
		} else if (!dthCadastro.equals(other.dthCadastro))
			return false;
		if (funcionalidadeId == null) {
			if (other.funcionalidadeId != null)
				return false;
		} else if (!funcionalidadeId.equals(other.funcionalidadeId))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (imei == null) {
			if (other.imei != null)
				return false;
		} else if (!imei.equals(other.imei))
			return false;
		if (modeloId == null) {
			if (other.modeloId != null)
				return false;
		} else if (!modeloId.equals(other.modeloId))
			return false;
		if (numeroGsm == null) {
			if (other.numeroGsm != null)
				return false;
		} else if (!numeroGsm.equals(other.numeroGsm))
			return false;
		return true;
	}

}
