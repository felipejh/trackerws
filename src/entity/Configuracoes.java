package entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CONFIGURACOES")
public class Configuracoes {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Integer id;

	@Column(name = "TEMPO_HISTORICO")
	private Integer tempoHistorico;
	
	@Column(name = "USUARIO_ID")
	private Integer usuarioId;

	@Column(name = "DTH_CADASTRO")
	private Date dthCadastro;

	@Column(name = "DTH_ATUALIZACAO")
	private Date dthAtualizacao;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTempoHistorico() {
		return tempoHistorico;
	}
	
	public void setTempoHistorico(Integer tempoHistorico) {
		this.tempoHistorico = tempoHistorico;
	}
	
	public Integer getUsuarioID() {
		return usuarioId;
	}
	
	public void setUsuarioID(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}

	public Date getDthCadastro() {
		return dthCadastro;
	}

	public void setDthCadastro(Date dthCadastro) {
		this.dthCadastro = dthCadastro;
	}

	public Date getDthAtualizacao() {
		return dthAtualizacao;
	}

	public void setDthAtualizacao(Date dthAtualizacao) {
		this.dthAtualizacao = dthAtualizacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dthAtualizacao == null) ? 0 : dthAtualizacao.hashCode());
		result = prime * result + ((dthCadastro == null) ? 0 : dthCadastro.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((tempoHistorico == null) ? 0 : tempoHistorico.hashCode());
		result = prime * result + ((usuarioId == null) ? 0 : usuarioId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Configuracoes other = (Configuracoes) obj;
		if (dthAtualizacao == null) {
			if (other.dthAtualizacao != null)
				return false;
		} else if (!dthAtualizacao.equals(other.dthAtualizacao))
			return false;
		if (dthCadastro == null) {
			if (other.dthCadastro != null)
				return false;
		} else if (!dthCadastro.equals(other.dthCadastro))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (tempoHistorico == null) {
			if (other.tempoHistorico != null)
				return false;
		} else if (!tempoHistorico.equals(other.tempoHistorico))
			return false;
		if (usuarioId == null) {
			if (other.usuarioId != null)
				return false;
		} else if (!usuarioId.equals(other.usuarioId))
			return false;
		return true;
	}

	
}
