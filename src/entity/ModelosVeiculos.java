package entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MODELOS_VEICULOS")
public class ModelosVeiculos {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Integer id;

	@Column(name = "DESCRICAO")
	private String descricao;

	@Column(name = "MARCAS_VEICULOS_ID")
	private Integer marcasVeiculosId;

	@Column(name = "DTH_CADASTRO")
	private Date dthCadastro;

	@Column(name = "DTH_ATUALIZACAO")
	private Date dthAtualizacao;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getMarcasVeiculosId() {
		return marcasVeiculosId;
	}

	public void setMarcasVeiculosId(Integer marcasVeiculosId) {
		this.marcasVeiculosId = marcasVeiculosId;
	}

	public Date getDthCadastro() {
		return dthCadastro;
	}

	public void setDthCadastro(Date dthCadastro) {
		this.dthCadastro = dthCadastro;
	}

	public Date getDthAtualizacao() {
		return dthAtualizacao;
	}

	public void setDthAtualizacao(Date dthAtualizacao) {
		this.dthAtualizacao = dthAtualizacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((dthAtualizacao == null) ? 0 : dthAtualizacao.hashCode());
		result = prime * result + ((dthCadastro == null) ? 0 : dthCadastro.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((marcasVeiculosId == null) ? 0 : marcasVeiculosId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ModelosVeiculos other = (ModelosVeiculos) obj;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (dthAtualizacao == null) {
			if (other.dthAtualizacao != null)
				return false;
		} else if (!dthAtualizacao.equals(other.dthAtualizacao))
			return false;
		if (dthCadastro == null) {
			if (other.dthCadastro != null)
				return false;
		} else if (!dthCadastro.equals(other.dthCadastro))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (marcasVeiculosId == null) {
			if (other.marcasVeiculosId != null)
				return false;
		} else if (!marcasVeiculosId.equals(other.marcasVeiculosId))
			return false;
		return true;
	}

}
