package listener;

import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import device.decoder.gt06.Gt06Decoder;
import device.encoder.Gt06Encoder;
import ejb.LogsRastreadorEJB;
import entity.LogEntradas;
import entity.LogLogin;
import util.Converter;

public class Gt06Listener {

  private static final Integer PORT_SOCKET = 50100; // Porta servidor
  //private static final Integer PORT_SOCKET = 50150; // Porta local
  private static final String TIP_LOGIN = "01";
  private static final String TIP_LOCALIZACAO = "12";
  private static final String TIP_STATUS = "13";
  private LogsRastreadorEJB logsRastreadorEJB;
  private final ExecutorService clientProcessingPool = Executors.newFixedThreadPool(1);

  public void startServer(LogsRastreadorEJB logsRastreadorEJB) {
    this.logsRastreadorEJB = logsRastreadorEJB;

    Runnable serverTask = new Runnable() {
      public void run() {
        try {
          ServerSocket serverSocket = new ServerSocket(PORT_SOCKET);
          System.out.println("Aguardando dados do rastreador GT06...");
          while (true) {
            try {
              Socket clientSocket = serverSocket.accept();
              clientProcessingPool.submit(new ClientTask(clientSocket));
            } catch (IOException e) {
              System.err.println("Erro na requisição");
              e.printStackTrace();
              serverSocket.close();
            }
          }
        } catch (Exception e) {
          System.out.println("Erro ao criar o servidor.");
        }
      }
    };
    Thread serverThread = new Thread(serverTask);
    serverThread.start();
  }

  private class ClientTask implements Runnable {

    private Socket sk;

    public ClientTask(Socket sk) {
      this.sk = sk;
    }

    @Override
    public void run() {
      System.out.println("Recebi o primeiro retorno!");
      try {

        byte[] buf = new byte[1024];
        do {
          Callable<Integer> cl = () -> sk.getInputStream().read(buf, 0, buf.length);
          ExecutorService service = Executors.newSingleThreadExecutor();
          Future<Integer> ft = service.submit(cl);

          if (ft.get(2, TimeUnit.MINUTES) < 0) {
            break;
          }
          OutputStream sockOutput = sk.getOutputStream();
          String packet = Converter.byteArrayToHex(buf).substring(0, buf.length);

          System.out.println(packet);

          Gt06Decoder gt06 = new Gt06Decoder(packet);
          SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

          if (TIP_LOGIN.equals(gt06.getTipPacket())) {

            System.out.println("Mensagem de login GT06:");

            System.out.println("Bit Inicial : " + gt06.getBitInicial());
            System.out.println("Tamanho do pacote: " + gt06.getTamanhoPacote());
            System.out.println("Tipo informação: " + gt06.getTipPacket());
            System.out.println("IMEI: " + gt06.getNumImei());
            System.out.println("Serial: " + gt06.getNumSerial());
            System.out.println("Error Check: " + gt06.getErrorCheck());
            System.out.println("Bit final: " + gt06.getBitFinal());

            LogLogin logLogin = new LogLogin();
            logLogin.setIp(sk.getRemoteSocketAddress().toString().substring(1));
            logLogin.setImei(gt06.getNumImei());
            logLogin.setNumSeqSerial(Integer.valueOf(gt06.getNumSerial()));
            logLogin.setDthCadastro(sdf.parse(sdf.format(new Date())));
            logLogin.setDthAtualizacao(sdf.parse(sdf.format(new Date())));

            logsRastreadorEJB.inserirLogLogin(logLogin);

            Gt06Encoder gt06Encoder = new Gt06Encoder(gt06);

            Thread.sleep(500);
            sockOutput.write(gt06Encoder.getMsgResponseLogin());
            sockOutput.flush();
          }

          if (TIP_LOCALIZACAO.equals(gt06.getTipPacket())) {

            System.out.println("MSG Localização GT06: " + gt06.getDataHora());
            // System.out.println("CANAL LOCALIZACAO: " + sk.getTrafficClass() + " - " +
            // sk.getLocalSocketAddress() + "- " + sk.getRemoteSocketAddress());

            System.out.println("Latitude: " + gt06.getLatitude());
            System.out.println("Longitude: " + gt06.getLongitude());

            LogEntradas logEntradas = new LogEntradas();
            logEntradas.setIp(sk.getRemoteSocketAddress().toString().substring(1));
            logEntradas.setLatitude(gt06.getLatitude());
            logEntradas.setLongitude(gt06.getLongitude());
            logEntradas.setQtdSatelites(gt06.getQtdSatelites());
            logEntradas.setVelocidade(gt06.getVelocidade());
            logEntradas.setDirecao(gt06.getDirecao());
            logEntradas.setMcc(gt06.getMcc());
            logEntradas.setMnc(gt06.getMnc());
            logEntradas.setLac(gt06.getLac());
            logEntradas.setTorreCelular(gt06.getTorreCelular());
            logEntradas.setNumSeqSerial(gt06.getNumSerial());
            logEntradas.setIndTempoReal(gt06.getIndTempoReal());
            logEntradas.setIndGpsPosicionado(gt06.getIndGpsPosicionado());
            logEntradas.setDthLocalização(sdf.parse(gt06.getDataHora()));
            logEntradas.setDthCadastro(sdf.parse(sdf.format(new Date())));
            logEntradas.setDthAtualizacao(sdf.parse(sdf.format(new Date())));

            logsRastreadorEJB.inserirLogEntradas(logEntradas);
          }

          if (TIP_STATUS.equals(gt06.getTipPacket())) {

            System.out.println("MSG Status: " + sdf.format(new Date()));

            Gt06Encoder gt06Encoder = new Gt06Encoder(gt06);

            Thread.sleep(500);
            sockOutput.write(gt06Encoder.getMsgResponseStatus());
            sockOutput.flush();
          }

        } while (true);
      } catch (TimeoutException e) {
        System.out.println("TIME OUT");
      } catch (Exception e) {
        e.printStackTrace(System.err);
      } finally {
        try {
          sk.close();
        } catch (IOException e) {

        }
      }

    }
  }
}
