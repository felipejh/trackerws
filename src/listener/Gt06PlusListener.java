package listener;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import device.decoder.gt06plus.Gt06PlusDecoder;
import device.decoder.gt06plus.Gt06PlusTipsPacket;
import ejb.LogsRastreadorEJB;
import entity.LogEntradas;

public class Gt06PlusListener {

  private static final Integer PORT_SOCKET = 50200; // Porta servidor
  //private static final Integer PORT_SOCKET = 50250; // Porta local
  private final ExecutorService clientProcessingPool = Executors.newFixedThreadPool(2);
  private LogsRastreadorEJB logsRastreadorEJB;

  public void startServer(LogsRastreadorEJB logsRastreadorEJB) {
    this.logsRastreadorEJB = logsRastreadorEJB;

    Runnable serverTask = new Runnable() {
      public void run() {
        try {
          ServerSocket serverSocket = new ServerSocket(PORT_SOCKET);
          System.out.println("Aguardando dados do rastreador GT06+...");
          while (true) {
            try {
              Socket clientSocket = serverSocket.accept();
              clientProcessingPool.submit(new ClientTask(clientSocket));
            } catch (IOException e) {
              System.err.println("Erro na requisição");
              e.printStackTrace();
              serverSocket.close();
            }
          }
        } catch (Exception e) {
          System.out.println("Erro ao criar o servidor.");
        }
      }
    };
    Thread serverThread = new Thread(serverTask);
    serverThread.start();
  }

  private class ClientTask implements Runnable {

    private Socket sk;

    public ClientTask(Socket sk) {
      this.sk = sk;
    }

    @Override
    public void run() {
      System.out.println("Recebi o primeiro retorno!");
      try {

        byte[] buf = new byte[1024];
        do {
          Callable<Integer> cl = () -> sk.getInputStream().read(buf, 0, buf.length);
          ExecutorService service = Executors.newSingleThreadExecutor();
          Future<Integer> ft = service.submit(cl);

          if (ft.get(2, TimeUnit.MINUTES) < 0) {
            break;
          }

          String packet = new String(buf, 0, buf.length);
          Gt06PlusDecoder decoder = new Gt06PlusDecoder(packet);
          if (Gt06PlusTipsPacket.TIP_PACKET_NORMAL_V1.getTipPacket()
              .equals(decoder.getTipPacket())) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            System.out.println(packet);
            System.out.println(decoder.getTipPacket());
            LogEntradas logEntradas = new LogEntradas();
            logEntradas.setIp(sk.getRemoteSocketAddress().toString().substring(1));
            logEntradas.setImei(decoder.getNumImei());
            logEntradas.setLatitude(decoder.getLatitude());
            logEntradas.setLongitude(decoder.getLongitude());
            logEntradas.setDthLocalização(sdf.parse(decoder.getDataHora()));
            logEntradas.setVelocidade(decoder.getVelocidade());
            logEntradas.setDirecao(decoder.getDirecao());
            logEntradas.setDthCadastro(sdf.parse(sdf.format(new Date())));
            logEntradas.setDthAtualizacao(sdf.parse(sdf.format(new Date())));

            logsRastreadorEJB.inserirLogEntradas(logEntradas);
            
          }
          
          
          
        } while (true);
      } catch (TimeoutException e) {
        System.out.println("TIME OUT");
      } catch (Exception e) {
        e.printStackTrace(System.err);
      } finally {
        try {
          sk.close();
        } catch (IOException e) {

        }
      }

    }
  }
}
