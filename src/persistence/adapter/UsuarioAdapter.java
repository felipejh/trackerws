package persistence.adapter;

import entity.Usuarios;
import vo.UsuarioVO;

public class UsuarioAdapter {

	public UsuarioVO entityToUsuarioVO(Usuarios usuarios) {

		UsuarioVO usuarioVO = new UsuarioVO();
		usuarioVO.setIdWS(usuarios.getId());
		usuarioVO.setNome(usuarios.getNome());
		usuarioVO.setEmail(usuarios.getEmail());
		usuarioVO.setSenha(null);
		usuarioVO.setToken(null);

		return usuarioVO;
	}
}
