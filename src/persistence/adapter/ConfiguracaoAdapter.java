package persistence.adapter;

import entity.Configuracoes;
import util.Converter;
import vo.ConfiguracaoVO;

public class ConfiguracaoAdapter {

	public ConfiguracaoVO entityToConfiguracaoVO(Configuracoes configuracoes) {

		ConfiguracaoVO configuracaoVO = new ConfiguracaoVO();
		configuracaoVO.setIdWS(configuracoes.getId());
		configuracaoVO.setTempoHistorico(configuracoes.getTempoHistorico());
		configuracaoVO.setDthCadastro(Converter.toString(configuracoes.getDthCadastro()));
		configuracaoVO.setDthAtualizacao(Converter.toString(configuracoes.getDthAtualizacao()));

		return configuracaoVO;
	}
}
