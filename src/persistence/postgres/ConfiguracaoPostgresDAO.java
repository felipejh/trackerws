package persistence.postgres;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import entity.Configuracoes;
import entity.Usuarios;
import persistence.ConfiguracaoDAO;
import persistence.adapter.ConfiguracaoAdapter;
import persistence.adapter.UsuarioAdapter;
import vo.ConfiguracaoVO;
import vo.UsuarioVO;

public class ConfiguracaoPostgresDAO implements ConfiguracaoDAO {

  private EntityManager em;

  public ConfiguracaoPostgresDAO(EntityManager em) {
    super();
    this.em = em;
  }

  @Override
  public List<ConfiguracaoVO> getConfiguracoesDthAtualizacao(Integer usuarioId,
      Date dthAtualizacao) throws ParseException {

    StringBuilder sql = new StringBuilder();
    sql.append(" SELECT id                                 ");
    sql.append("      , tempo_historico                    ");
    sql.append("      , usuario_id                         ");
    sql.append("      , dth_cadastro                       ");
    sql.append("      , dth_atualizacao                    ");
    sql.append("   FROM configuracoes                      ");
    sql.append("  WHERE dth_atualizacao >= :par_dthAtualizacao ");
    sql.append("    AND usuario_id       = :par_usuarioId      ");

    Query query = this.em.createNativeQuery(sql.toString());

    Map<String, Object> pars = new HashMap<String, Object>();
    pars.put("par_dthAtualizacao", dthAtualizacao);
    pars.put("par_usuarioId", usuarioId);
    pars.forEach((chave, valor) -> query.setParameter(chave, valor));

    List<?> configuracoes = query.getResultList();

    ArrayList<ConfiguracaoVO> listaConfiguracoesVO = null;

    if (configuracoes != null && !configuracoes.isEmpty()) {

      listaConfiguracoesVO = new ArrayList<ConfiguracaoVO>();
      ConfiguracaoAdapter configuracaoAdapter = new ConfiguracaoAdapter();
      UsuarioAdapter usuarioAdapter = new UsuarioAdapter();

      for (Object row : configuracoes) {
        Object[] rowArray = (Object[]) row;
        
        Configuracoes configuracao = new Configuracoes();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        configuracao.setId(Integer.valueOf(rowArray[0].toString()));
        configuracao.setTempoHistorico(Integer.valueOf(rowArray[1].toString()));
        configuracao.setUsuarioID(Integer.valueOf(rowArray[2].toString()));
        configuracao.setDthCadastro(sdf.parse(rowArray[3].toString()));
        configuracao.setDthAtualizacao(sdf.parse(rowArray[4].toString()));
            
        ConfiguracaoVO configuracaoVO = configuracaoAdapter.entityToConfiguracaoVO(configuracao);

        // Dados do usuário
        Usuarios usuario = this.em.find(Usuarios.class, configuracao.getUsuarioID());
        UsuarioVO usuarioVO = usuarioAdapter.entityToUsuarioVO(usuario);
        configuracaoVO.setUsuario(usuarioVO);

        listaConfiguracoesVO.add(configuracaoVO);

      }

    }

    return listaConfiguracoesVO;
  }

}
