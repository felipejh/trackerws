package persistence;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import vo.ConfiguracaoVO;

public interface ConfiguracaoDAO {

	public List<ConfiguracaoVO> getConfiguracoesDthAtualizacao(Integer usuarioId, Date dthAtualizacao) throws ParseException;

}
