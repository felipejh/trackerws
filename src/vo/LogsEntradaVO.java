package vo;

public class LogsEntradaVO {

	private Integer idWS;
	private Double latitude;
	private Double longitude;
	private String dthLocalizacao;
	private Integer velocidade;
	private Integer direcao;
	private VeiculosUsuarioVO veiculo;

	public Integer getIdWS() {
		return idWS;
	}

	public void setIdWS(Integer idWS) {
		this.idWS = idWS;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getDthLocalizacao() {
		return dthLocalizacao;
	}

	public void setDthLocalizacao(String dthLocalizacao) {
		this.dthLocalizacao = dthLocalizacao;
	}

	public Integer getVelocidade() {
		return velocidade;
	}

	public void setVelocidade(Integer velocidade) {
		this.velocidade = velocidade;
	}

	public Integer getDirecao() {
		return direcao;
	}

	public void setDirecao(Integer direcao) {
		this.direcao = direcao;
	}

	public VeiculosUsuarioVO getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(VeiculosUsuarioVO veiculoUsuario) {
		this.veiculo = veiculoUsuario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((direcao == null) ? 0 : direcao.hashCode());
		result = prime * result + ((dthLocalizacao == null) ? 0 : dthLocalizacao.hashCode());
		result = prime * result + ((idWS == null) ? 0 : idWS.hashCode());
		result = prime * result + ((latitude == null) ? 0 : latitude.hashCode());
		result = prime * result + ((longitude == null) ? 0 : longitude.hashCode());
		result = prime * result + ((veiculo == null) ? 0 : veiculo.hashCode());
		result = prime * result + ((velocidade == null) ? 0 : velocidade.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LogsEntradaVO other = (LogsEntradaVO) obj;
		if (direcao == null) {
			if (other.direcao != null)
				return false;
		} else if (!direcao.equals(other.direcao))
			return false;
		if (dthLocalizacao == null) {
			if (other.dthLocalizacao != null)
				return false;
		} else if (!dthLocalizacao.equals(other.dthLocalizacao))
			return false;
		if (idWS == null) {
			if (other.idWS != null)
				return false;
		} else if (!idWS.equals(other.idWS))
			return false;
		if (latitude == null) {
			if (other.latitude != null)
				return false;
		} else if (!latitude.equals(other.latitude))
			return false;
		if (longitude == null) {
			if (other.longitude != null)
				return false;
		} else if (!longitude.equals(other.longitude))
			return false;
		if (veiculo == null) {
			if (other.veiculo != null)
				return false;
		} else if (!veiculo.equals(other.veiculo))
			return false;
		if (velocidade == null) {
			if (other.velocidade != null)
				return false;
		} else if (!velocidade.equals(other.velocidade))
			return false;
		return true;
	}

}
