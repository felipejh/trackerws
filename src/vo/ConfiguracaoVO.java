package vo;

public class ConfiguracaoVO {

	private Integer idWS;
	private Integer tempoHistorico;
	private String dthCadastro;
	private String dthAtualizacao;
	private UsuarioVO usuario;
	
	public Integer getIdWS() {
		return idWS;
	}
	public void setIdWS(Integer idWS) {
		this.idWS = idWS;
	}
	public Integer getTempoHistorico() {
		return tempoHistorico;
	}
	public void setTempoHistorico(Integer tempoHistorico) {
		this.tempoHistorico = tempoHistorico;
	}
	public String getDthCadastro() {
		return dthCadastro;
	}
	public void setDthCadastro(String dthCadastro) {
		this.dthCadastro = dthCadastro;
	}
	public String getDthAtualizacao() {
		return dthAtualizacao;
	}
	public void setDthAtualizacao(String dthAtualizacao) {
		this.dthAtualizacao = dthAtualizacao;
	}
	public UsuarioVO getUsuario() {
		return usuario;
	}
	public void setUsuario(UsuarioVO usuario) {
		this.usuario = usuario;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dthAtualizacao == null) ? 0 : dthAtualizacao.hashCode());
		result = prime * result + ((dthCadastro == null) ? 0 : dthCadastro.hashCode());
		result = prime * result + ((idWS == null) ? 0 : idWS.hashCode());
		result = prime * result + ((tempoHistorico == null) ? 0 : tempoHistorico.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConfiguracaoVO other = (ConfiguracaoVO) obj;
		if (dthAtualizacao == null) {
			if (other.dthAtualizacao != null)
				return false;
		} else if (!dthAtualizacao.equals(other.dthAtualizacao))
			return false;
		if (dthCadastro == null) {
			if (other.dthCadastro != null)
				return false;
		} else if (!dthCadastro.equals(other.dthCadastro))
			return false;
		if (idWS == null) {
			if (other.idWS != null)
				return false;
		} else if (!idWS.equals(other.idWS))
			return false;
		if (tempoHistorico == null) {
			if (other.tempoHistorico != null)
				return false;
		} else if (!tempoHistorico.equals(other.tempoHistorico))
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		return true;
	}
	
	
}
