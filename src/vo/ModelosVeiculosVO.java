package vo;

public class ModelosVeiculosVO {

	private Integer idWS;
	private String descricao;
	private MarcasVeiculosVO marcaVeiculo;

	public Integer getIdWS() {
		return idWS;
	}

	public void setIdWS(Integer idWS) {
		this.idWS = idWS;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public MarcasVeiculosVO getMarcaVeiculo() {
		return marcaVeiculo;
	}

	public void setMarcaVeiculo(MarcasVeiculosVO marcaVeiculo) {
		this.marcaVeiculo = marcaVeiculo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((idWS == null) ? 0 : idWS.hashCode());
		result = prime * result + ((marcaVeiculo == null) ? 0 : marcaVeiculo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ModelosVeiculosVO other = (ModelosVeiculosVO) obj;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (idWS == null) {
			if (other.idWS != null)
				return false;
		} else if (!idWS.equals(other.idWS))
			return false;
		if (marcaVeiculo == null) {
			if (other.marcaVeiculo != null)
				return false;
		} else if (!marcaVeiculo.equals(other.marcaVeiculo))
			return false;
		return true;
	}

}
