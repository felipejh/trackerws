package vo;

public class VeiculosUsuarioVO {

	private Integer idWS;
	private String placa;
	private Integer ano;
	private String cor;
	private String nomeCondutor;
	private Integer indAtivo;
	private String dthCadastro;
	private String dthAtualizacao;
	private UsuarioVO usuario;
	private ModelosVeiculosVO modeloVeiculo;

	public Integer getIdWS() {
		return idWS;
	}

	public void setIdWS(Integer idWS) {
		this.idWS = idWS;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public String getNomeCondutor() {
		return nomeCondutor;
	}

	public void setNomeCondutor(String nomeCondutor) {
		this.nomeCondutor = nomeCondutor;
	}

	public Integer getIndAtivo() {
		return indAtivo;
	}

	public void setIndAtivo(Integer indAtivo) {
		this.indAtivo = indAtivo;
	}

	public String getDthCadastro() {
		return dthCadastro;
	}

	public void setDthCadastro(String dthCadastro) {
		this.dthCadastro = dthCadastro;
	}

	public String getDthAtualizacao() {
		return dthAtualizacao;
	}

	public void setDthAtualizacao(String dthAtualizacao) {
		this.dthAtualizacao = dthAtualizacao;
	}

	public UsuarioVO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioVO usuarioVO) {
		this.usuario = usuarioVO;
	}

	public ModelosVeiculosVO getModeloVeiculo() {
		return modeloVeiculo;
	}

	public void setModeloVeiculo(ModelosVeiculosVO modeloVeiculoVO) {
		this.modeloVeiculo = modeloVeiculoVO;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ano == null) ? 0 : ano.hashCode());
		result = prime * result + ((cor == null) ? 0 : cor.hashCode());
		result = prime * result + ((dthAtualizacao == null) ? 0 : dthAtualizacao.hashCode());
		result = prime * result + ((dthCadastro == null) ? 0 : dthCadastro.hashCode());
		result = prime * result + ((idWS == null) ? 0 : idWS.hashCode());
		result = prime * result + ((indAtivo == null) ? 0 : indAtivo.hashCode());
		result = prime * result + ((modeloVeiculo == null) ? 0 : modeloVeiculo.hashCode());
		result = prime * result + ((nomeCondutor == null) ? 0 : nomeCondutor.hashCode());
		result = prime * result + ((placa == null) ? 0 : placa.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VeiculosUsuarioVO other = (VeiculosUsuarioVO) obj;
		if (ano == null) {
			if (other.ano != null)
				return false;
		} else if (!ano.equals(other.ano))
			return false;
		if (cor == null) {
			if (other.cor != null)
				return false;
		} else if (!cor.equals(other.cor))
			return false;
		if (dthAtualizacao == null) {
			if (other.dthAtualizacao != null)
				return false;
		} else if (!dthAtualizacao.equals(other.dthAtualizacao))
			return false;
		if (dthCadastro == null) {
			if (other.dthCadastro != null)
				return false;
		} else if (!dthCadastro.equals(other.dthCadastro))
			return false;
		if (idWS == null) {
			if (other.idWS != null)
				return false;
		} else if (!idWS.equals(other.idWS))
			return false;
		if (indAtivo == null) {
			if (other.indAtivo != null)
				return false;
		} else if (!indAtivo.equals(other.indAtivo))
			return false;
		if (modeloVeiculo == null) {
			if (other.modeloVeiculo != null)
				return false;
		} else if (!modeloVeiculo.equals(other.modeloVeiculo))
			return false;
		if (nomeCondutor == null) {
			if (other.nomeCondutor != null)
				return false;
		} else if (!nomeCondutor.equals(other.nomeCondutor))
			return false;
		if (placa == null) {
			if (other.placa != null)
				return false;
		} else if (!placa.equals(other.placa))
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		return true;
	}

}
