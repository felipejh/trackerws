package vo;

public class TiposVeiculosVO {

	private Integer idWS;
	private String descricao;

	public Integer getIdWS() {
		return idWS;
	}

	public void setIdWS(Integer id) {
		this.idWS = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((idWS == null) ? 0 : idWS.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TiposVeiculosVO other = (TiposVeiculosVO) obj;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (idWS == null) {
			if (other.idWS != null)
				return false;
		} else if (!idWS.equals(other.idWS))
			return false;
		return true;
	}

}
