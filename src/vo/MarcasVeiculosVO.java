package vo;

public class MarcasVeiculosVO {

	private Integer idWS;
	private String descricao;
	private TiposVeiculosVO tipoVeiculo;

	public Integer getIdWS() {
		return idWS;
	}

	public void setIdWS(Integer idWS) {
		this.idWS = idWS;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public TiposVeiculosVO getTipoVeiculo() {
		return tipoVeiculo;
	}

	public void setTipoVeiculo(TiposVeiculosVO tipoVeiculo) {
		this.tipoVeiculo = tipoVeiculo;
	}

}
