package ejb;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import entity.LogEntradas;
import entity.LogLogin;

@Stateless
@LocalBean
public class LogsRastreadorEJB {

	@PersistenceContext(unitName = "exemplo-pu")
	private EntityManager em;

	public void inserirLogLogin(LogLogin logLogin) {

		this.em.persist(logLogin);

	}

	public void inserirLogEntradas(LogEntradas logEntradas) {

		if (logEntradas.getImei() == null) {
			Query query = this.em.createNativeQuery("SELECT imei FROM log_login WHERE ip = :ip ORDER BY id DESC LIMIT 1");
			query.setParameter("ip", logEntradas.getIp());
			List<?> results = query.getResultList();

			if (results != null && !results.isEmpty()) {

				for (Object row : results) {

					logEntradas.setImei(Long.valueOf(row.toString()));

				}
			}
		}
		this.em.persist(logEntradas);
	}

}
