package ejb;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.mail.internet.ParseException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.UriInfo;
import auth.SimpleKeyGenerator;
import exception.AuthInvalidaExpiradaException;
import io.jsonwebtoken.Jwts;
import persistence.postgres.ConfiguracaoPostgresDAO;
import vo.ConfiguracaoVO;
import vo.response.CustomResponse;
import vo.response.ResponseStatus;

@Stateless
@LocalBean
@Path("/configuracao")
public class ConfiguracaoEJB {

	@PersistenceContext(unitName = "exemplo-pu")
	private EntityManager em;

	@GET
	@Path("/listaConfiguracoesAtualizadas")
	@Produces("application/json")
	public CustomResponse listaTipoVeiculosAtualizados(@Context UriInfo info, @Context HttpHeaders header)
			throws ParseException, java.text.ParseException, AuthInvalidaExpiradaException {

		String dataAt = info.getQueryParameters().getFirst("dataAt");
		Integer usuarioId = Integer.valueOf(info.getQueryParameters().getFirst("usuarioID"));
		CustomResponse response = new CustomResponse();

		try {
			String token = header.getHeaderString("Authorization");
			Jwts.parser().setSigningKey(new SimpleKeyGenerator().generateKey()).parseClaimsJws(token).getBody();
		} catch (Exception e) {
			response.setCod(401);
			response.setStatus(ResponseStatus.ERROR);
			response.setMessage("Autenticação inválida ou expirada.");
			response.setData(null);
			return response;
		}
		try {
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date dtaFormat = sdf.parse(dataAt);

		ConfiguracaoPostgresDAO configuracaoPostgresDAO = new ConfiguracaoPostgresDAO(this.em);
		
		List<ConfiguracaoVO> listaConfiguracoesVO = configuracaoPostgresDAO.getConfiguracoesDthAtualizacao(usuarioId, dtaFormat);


		if (listaConfiguracoesVO != null && !listaConfiguracoesVO.isEmpty()) {

			response.setCod(200);
			response.setStatus(ResponseStatus.SUCCESS);
			response.setMessage("Sucesso");
			response.setData(listaConfiguracoesVO);

		} else {
			response.setCod(200);
			response.setStatus(ResponseStatus.ERROR);
			response.setMessage("Nenhum registro encontrado");
		}
		
	      } catch (Exception e) {
	          e.printStackTrace();
	        }
		
		return response;
	}

}