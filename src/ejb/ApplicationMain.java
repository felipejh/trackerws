package ejb;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/api")
public class ApplicationMain extends Application {

  @Override
  public Set<Class<?>> getClasses() {
    Set<Class<?>> classes = new HashSet<Class<?>>();
    classes.add(UsuarioEJB.class);
    classes.add(VeiculoEJB.class);
    classes.add(ConfiguracaoEJB.class);
    classes.add(ExceptionHandler.class);
    return classes;
  }
}
