package ejb;

import java.security.Key;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.mail.internet.ParseException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.UriInfo;

import auth.SimpleKeyGenerator;
import entity.Usuarios;
import exception.AuthInvalidaExpiradaException;
import exception.UsuarioBloqueadoException;
import exception.UsuarioSenhaIncorretosException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import vo.UsuarioVO;
import vo.response.CustomResponse;
import vo.response.ResponseStatus;

@Stateless
@LocalBean
@Path("/usuario")
public class UsuarioEJB {

	@PersistenceContext(unitName = "exemplo-pu")
	private EntityManager em;

	private String issueToken(String login) {

		Date d = new Date();

		Calendar c = Calendar.getInstance();
		c.setTime(d);

		c.set(Calendar.DAY_OF_MONTH, c.get(Calendar.DAY_OF_MONTH) + 10);
		c.set(Calendar.MONTH, c.get(Calendar.MONTH) + 1);
		c.set(Calendar.YEAR, c.get(Calendar.YEAR) + 1);

		Key key = new SimpleKeyGenerator().generateKey();
		String jwtToken = Jwts.builder().setSubject(login).setIssuer("Tracker").setIssuedAt(new Date()).setExpiration(
				new Date(new Date().getTime() + c.getTimeInMillis())).signWith(SignatureAlgorithm.HS512, key).compact();
		return jwtToken;

	}

	@GET
	@Path("/login")
	@Produces("application/json")
	public CustomResponse login(@Context UriInfo info) throws ParseException, UsuarioSenhaIncorretosException {

		String email = info.getQueryParameters().getFirst("email");
		String senha = info.getQueryParameters().getFirst("senha");

		CustomResponse response = new CustomResponse();
		UsuarioVO usuario = new UsuarioVO();

		Query query = this.em.createNativeQuery("SELECT id, nome FROM usuarios WHERE email = :email AND senha = :senha");
		query.setParameter("email", email);
		query.setParameter("senha", senha);

		List<?> results = query.getResultList();

		if (results != null && !results.isEmpty()) {

			for (Object row : results) {
				Object[] rowArray = (Object[]) row;
				usuario.setNome(rowArray[1].toString());
				usuario.setIdWS(Integer.valueOf(rowArray[0].toString()));
				usuario.setEmail(email);

				// Issue a token for the user
				String token = this.issueToken(email);
				usuario.setToken(token);

				response.setCod(200);
				response.setStatus(ResponseStatus.SUCCESS);
				response.setMessage("Usuário autenticado com sucesso.");
				response.setData(usuario);
			}
		} else {
			response.setCod(401);
			response.setStatus(ResponseStatus.ERROR);
			response.setMessage("Usuário ou senha incorretos.");
			response.setData(null);
			return response;
		}
		return response;
	}

	@GET
	@Path("/validaLogin")
	@Produces("application/json")
	public CustomResponse validaLogin(@Context UriInfo info, @Context HttpHeaders header)
			throws AuthInvalidaExpiradaException, UsuarioBloqueadoException {
		CustomResponse response = new CustomResponse();

		Integer usuarioId = Integer.valueOf(info.getQueryParameters().getFirst("usuarioID"));

		try {
			String token = header.getHeaderString("Authorization");
			Claims claims = Jwts.parser().setSigningKey(new SimpleKeyGenerator().generateKey()).parseClaimsJws(token).getBody();

			String desEmail = claims.getSubject();
			Usuarios usuario = this.em.find(Usuarios.class, usuarioId);

			if (usuario != null && desEmail.equals(usuario.getEmail())) {
				response.setCod(200);
				response.setStatus(ResponseStatus.SUCCESS);
				response.setMessage("Usuário autenticado.");
			} else {
				response.setCod(401);
				response.setStatus(ResponseStatus.ERROR);
				response.setMessage("Usuário bloqueado na aplicação.");
				response.setData(null);
				return response;
			}

		} catch (Exception e) {
			response.setCod(401);
			response.setStatus(ResponseStatus.ERROR);
			response.setMessage("Autenticação inválida ou expirada.");
			response.setData(null);
			return response;
		}
		return response;

	}
}
