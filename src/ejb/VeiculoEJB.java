package ejb;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.mail.internet.ParseException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.UriInfo;

import auth.SimpleKeyGenerator;
import entity.Condutores;
import entity.Dispositivos;
import entity.MarcasVeiculos;
import entity.ModelosVeiculos;
import entity.TiposVeiculos;
import entity.Usuarios;
import exception.AuthInvalidaExpiradaException;
import io.jsonwebtoken.Jwts;
import vo.LogsEntradaVO;
import vo.MarcasVeiculosVO;
import vo.ModelosVeiculosVO;
import vo.TiposVeiculosVO;
import vo.UsuarioVO;
import vo.VeiculosUsuarioVO;
import vo.response.CustomResponse;
import vo.response.ResponseStatus;

@Stateless
@LocalBean
@Path("/veiculo")
public class VeiculoEJB {

	@PersistenceContext(unitName = "exemplo-pu")
	private EntityManager em;

	@GET
	@Path("/listaTipoVeiculosAtualizados")
	@Produces("application/json")
	public CustomResponse listaTipoVeiculosAtualizados(@Context UriInfo info, @Context HttpHeaders header)
			throws ParseException, java.text.ParseException, AuthInvalidaExpiradaException {

		String dataAt = info.getQueryParameters().getFirst("dataAt");
		CustomResponse response = new CustomResponse();

		try {
			String token = header.getHeaderString("Authorization");
			Jwts.parser().setSigningKey(new SimpleKeyGenerator().generateKey()).parseClaimsJws(token).getBody();
		} catch (Exception e) {
			response.setCod(401);
			response.setStatus(ResponseStatus.ERROR);
			response.setMessage("Autenticação inválida ou expirada.");
			response.setData(null);
			return response;
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date dtaFormat = sdf.parse(dataAt);

		Query query = this.em.createNativeQuery("SELECT id, descricao FROM tipos_veiculos WHERE dth_atualizacao >= :dthAtualizacao");
		query.setParameter("dthAtualizacao", dtaFormat);

		List<?> results = query.getResultList();

		if (results != null && !results.isEmpty()) {

			ArrayList<TiposVeiculosVO> listaTiposVeiculos = new ArrayList<TiposVeiculosVO>();
			for (Object row : results) {
				Object[] rowArray = (Object[]) row;

				TiposVeiculosVO tipoVeiculoResponse = new TiposVeiculosVO();
				tipoVeiculoResponse.setIdWS(Integer.valueOf(rowArray[0].toString()));
				tipoVeiculoResponse.setDescricao(rowArray[1].toString());

				listaTiposVeiculos.add(tipoVeiculoResponse);

			}

			response.setCod(200);
			response.setStatus(ResponseStatus.SUCCESS);
			response.setMessage("Sucesso");
			response.setData(listaTiposVeiculos);

		} else {
			response.setCod(200);
			response.setStatus(ResponseStatus.ERROR);
			response.setMessage("Nenhum registro encontrado");
		}
		return response;
	}

	@GET
	@Path("/listaMarcaVeiculosAtualizados")
	@Produces("application/json")
	public CustomResponse listaMarcaVeiculosAtualizados(@Context UriInfo info, @Context HttpHeaders header)
			throws ParseException, java.text.ParseException, AuthInvalidaExpiradaException {

		String dataAt = info.getQueryParameters().getFirst("dataAt");
		CustomResponse response = new CustomResponse();

		try {
			String token = header.getHeaderString("Authorization");
			Jwts.parser().setSigningKey(new SimpleKeyGenerator().generateKey()).parseClaimsJws(token).getBody();
		} catch (Exception e) {
			response.setCod(401);
			response.setStatus(ResponseStatus.ERROR);
			response.setMessage("Autenticação inválida ou expirada.");
			response.setData(null);
			return response;
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date dtaFormat = sdf.parse(dataAt);

		Query queryMarcasVeiculos = this.em.createNativeQuery(
				"SELECT id, descricao, tipo_veiculo_id FROM marcas_veiculos WHERE dth_atualizacao >= :dthAtualizacao");
		queryMarcasVeiculos.setParameter("dthAtualizacao", dtaFormat);

		List<?> resultsMarcasVeiculos = queryMarcasVeiculos.getResultList();

		if (resultsMarcasVeiculos != null && !resultsMarcasVeiculos.isEmpty()) {

			ArrayList<MarcasVeiculosVO> listaMarcasVeiculos = new ArrayList<MarcasVeiculosVO>();
			for (Object rowMarcas : resultsMarcasVeiculos) {
				Object[] rowMarcasArray = (Object[]) rowMarcas;

				MarcasVeiculosVO marcasVeiculosVO = new MarcasVeiculosVO();
				marcasVeiculosVO.setIdWS(Integer.valueOf(rowMarcasArray[0].toString()));
				marcasVeiculosVO.setDescricao(rowMarcasArray[1].toString());

				// Busca os tipos de veículos da marca
				TiposVeiculosVO tiposVeiculosVO = new TiposVeiculosVO();
				TiposVeiculos tiposVeiculos = this.em.find(TiposVeiculos.class, Integer.valueOf(rowMarcasArray[2].toString()));

				tiposVeiculosVO.setIdWS(tiposVeiculos.getId());
				tiposVeiculosVO.setDescricao(tiposVeiculos.getDescricao());

				marcasVeiculosVO.setTipoVeiculo(tiposVeiculosVO);
				listaMarcasVeiculos.add(marcasVeiculosVO);
			}

			response.setCod(200);
			response.setStatus(ResponseStatus.SUCCESS);
			response.setMessage("Consulta efetuada com sucesso.");
			response.setData(listaMarcasVeiculos);

		} else {
			response.setCod(200);
			response.setStatus(ResponseStatus.ERROR);
			response.setMessage("Nenhum registro encontrado");
		}
		return response;
	}

	@GET
	@Path("/listaModelosVeiculosAtualizados")
	@Produces("application/json")
	public CustomResponse listaModelosVeiculosAtualizados(@Context UriInfo info, @Context HttpHeaders header)
			throws ParseException, java.text.ParseException, AuthInvalidaExpiradaException {

		String dataAt = info.getQueryParameters().getFirst("dataAt");
		CustomResponse response = new CustomResponse();

		try {
			String token = header.getHeaderString("Authorization");
			Jwts.parser().setSigningKey(new SimpleKeyGenerator().generateKey()).parseClaimsJws(token).getBody();
		} catch (Exception e) {
			response.setCod(401);
			response.setStatus(ResponseStatus.ERROR);
			response.setMessage("Autenticação inválida ou expirada.");
			response.setData(null);
			return response;
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date dtaFormat = sdf.parse(dataAt);

		Query queryModelosVeiculos = this.em.createNativeQuery(
				"SELECT id, descricao, marcas_veiculos_id FROM modelos_veiculos WHERE dth_atualizacao >= :dthAtualizacao");
		queryModelosVeiculos.setParameter("dthAtualizacao", dtaFormat);

		List<?> resultsModelosVeiculos = queryModelosVeiculos.getResultList();
		if (resultsModelosVeiculos != null && !resultsModelosVeiculos.isEmpty()) {

			ArrayList<ModelosVeiculosVO> listaModelosVeiculos = new ArrayList<ModelosVeiculosVO>();
			for (Object rowModelos : resultsModelosVeiculos) {
				Object[] rowModelosArray = (Object[]) rowModelos;

				ModelosVeiculosVO modelosVeiculosVO = new ModelosVeiculosVO();
				modelosVeiculosVO.setIdWS(Integer.valueOf(rowModelosArray[0].toString()));
				modelosVeiculosVO.setDescricao(rowModelosArray[1].toString());

				// Busca a marca do modelo
				MarcasVeiculosVO marcasVeiculosVO = new MarcasVeiculosVO();
				MarcasVeiculos marcaVeiculo = this.em.find(MarcasVeiculos.class, Integer.valueOf(rowModelosArray[2].toString()));
				marcasVeiculosVO.setIdWS(marcaVeiculo.getId());
				marcasVeiculosVO.setDescricao(marcaVeiculo.getDescricao());

				// Busca o tipo de veículo da marca
				TiposVeiculosVO tiposVeiculosVO = new TiposVeiculosVO();
				TiposVeiculos tiposVeiculos = this.em.find(TiposVeiculos.class, marcaVeiculo.getTipoVeiculoId());

				tiposVeiculosVO.setIdWS(tiposVeiculos.getId());
				tiposVeiculosVO.setDescricao(tiposVeiculos.getDescricao());

				marcasVeiculosVO.setTipoVeiculo(tiposVeiculosVO);
				modelosVeiculosVO.setMarcaVeiculo(marcasVeiculosVO);

				listaModelosVeiculos.add(modelosVeiculosVO);
			}

			response.setCod(200);
			response.setStatus(ResponseStatus.SUCCESS);
			response.setMessage("Consulta efetuada com sucesso.");
			response.setData(listaModelosVeiculos);

		} else {
			response.setCod(200);
			response.setStatus(ResponseStatus.ERROR);
			response.setMessage("Nenhum registro encontrado");
		}
		return response;
	}

	@GET
	@Path("/listaVeiculosAtualizados")
	@Produces("application/json")
	public CustomResponse listaVeiculosAtualizados(@Context UriInfo info, @Context HttpHeaders header)
			throws java.text.ParseException, AuthInvalidaExpiradaException {

		CustomResponse response = new CustomResponse();
		String dataAt = info.getQueryParameters().getFirst("dataAt");
		Integer usuarioId = Integer.valueOf(info.getQueryParameters().getFirst("usuarioID"));

		try {
			String token = header.getHeaderString("Authorization");
			Jwts.parser().setSigningKey(new SimpleKeyGenerator().generateKey()).parseClaimsJws(token).getBody();
		} catch (Exception e) {
			response.setCod(401);
			response.setStatus(ResponseStatus.ERROR);
			response.setMessage("Autenticação inválida ou expirada.");
			response.setData(null);
			return response;
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date dtaFormat = sdf.parse(dataAt);

		String dta2 = "'" + sdf.format(dtaFormat) + "'";

		ArrayList<VeiculosUsuarioVO> listaVeiculosUsuario = new ArrayList<VeiculosUsuarioVO>();

		StringBuilder sqlVeiculos = new StringBuilder();
		sqlVeiculos.append("SELECT id "); // 0
		sqlVeiculos.append("      ,placa "); // 1
		sqlVeiculos.append("      ,ano "); // 2
		sqlVeiculos.append("      ,cor "); // 3
		sqlVeiculos.append("      ,condutor_id "); // 4
		sqlVeiculos.append("      ,modelo_id "); // 5
		sqlVeiculos.append("      ,ind_ativo "); // 6
		sqlVeiculos.append("      ,dth_cadastro "); // 7
		sqlVeiculos.append("      ,dth_atualizacao "); // 8
		sqlVeiculos.append("  FROM veiculos ");
		sqlVeiculos.append(" WHERE usuario_id = " + usuarioId);
		sqlVeiculos.append("   AND dth_atualizacao >= " + dta2);

		Query queryVeiculos = this.em.createNativeQuery(sqlVeiculos.toString());

		List<?> resultsVeiculos = queryVeiculos.getResultList();

		if (resultsVeiculos != null && !resultsVeiculos.isEmpty()) {

			for (Object rowVeiculos : resultsVeiculos) {
				Object[] rowVeiculosArray = (Object[]) rowVeiculos;

				VeiculosUsuarioVO veiculosUsuario = new VeiculosUsuarioVO();
				veiculosUsuario.setIdWS(Integer.valueOf(rowVeiculosArray[0].toString()));
				veiculosUsuario.setPlaca(rowVeiculosArray[1].toString());
				veiculosUsuario.setAno(Integer.valueOf(rowVeiculosArray[2].toString()));
				veiculosUsuario.setCor(rowVeiculosArray[3].toString());
				veiculosUsuario.setIndAtivo(Integer.valueOf(rowVeiculosArray[6].toString()));
				veiculosUsuario.setDthCadastro(sdf.format(sdf.parse(rowVeiculosArray[7].toString())));
				veiculosUsuario.setDthAtualizacao(sdf.format(sdf.parse(rowVeiculosArray[8].toString())));

				// Dados do condutor
				Integer condutorId = Integer.valueOf(rowVeiculosArray[4].toString());
				Condutores condutor = this.em.find(Condutores.class, condutorId);
				veiculosUsuario.setNomeCondutor(condutor.getNome());

				// Dados do usuário
				Usuarios usuario = this.em.find(Usuarios.class, usuarioId);
				UsuarioVO usuarioVO = new UsuarioVO();
				usuarioVO.setIdWS(usuario.getId());
				usuarioVO.setNome(usuario.getNome());
				usuarioVO.setEmail(usuario.getEmail());
				usuarioVO.setSenha(null);
				usuarioVO.setToken("12345");
				veiculosUsuario.setUsuario(usuarioVO);

				// Dados do modelo do veículo
				Integer modeloId = Integer.valueOf(rowVeiculosArray[5].toString());
				ModelosVeiculos modeloVeiculo = this.em.find(ModelosVeiculos.class, modeloId);

				ModelosVeiculosVO modelosVeiculosVO = new ModelosVeiculosVO();
				modelosVeiculosVO.setIdWS(modeloVeiculo.getId());
				modelosVeiculosVO.setDescricao(modeloVeiculo.getDescricao());

				// Busca a marca do modelo
				Integer marcaId = modeloVeiculo.getMarcasVeiculosId();
				MarcasVeiculos marcaVeiculo = this.em.find(MarcasVeiculos.class, marcaId);

				MarcasVeiculosVO marcaVeiculoVO = new MarcasVeiculosVO();
				marcaVeiculoVO.setIdWS(marcaVeiculo.getId());
				marcaVeiculoVO.setDescricao(marcaVeiculo.getDescricao());

				// Busca tipo do veículo
				Integer tipoVeiculoId = marcaVeiculo.getTipoVeiculoId();
				TiposVeiculos tipoVeiculo = this.em.find(TiposVeiculos.class, tipoVeiculoId);

				TiposVeiculosVO tipoVeiculoVO = new TiposVeiculosVO();
				tipoVeiculoVO.setIdWS(tipoVeiculo.getId());
				tipoVeiculoVO.setDescricao(tipoVeiculo.getDescricao());

				marcaVeiculoVO.setTipoVeiculo(tipoVeiculoVO);
				modelosVeiculosVO.setMarcaVeiculo(marcaVeiculoVO);
				veiculosUsuario.setModeloVeiculo(modelosVeiculosVO);

				listaVeiculosUsuario.add(veiculosUsuario);
			}
			response.setCod(200);
			response.setStatus(ResponseStatus.SUCCESS);
			response.setMessage("Consulta efetuada com sucesso");
			response.setData(listaVeiculosUsuario);

		} else {
			response.setCod(404);
			response.setStatus(ResponseStatus.ERROR);
			response.setMessage("Erro");
		}
		return response;
	}

	@GET
	@Path("/listaLogsEntradasAtualizados")
	@Produces("application/json")
	public CustomResponse listaLogsEntradasAtualizados(@Context UriInfo info, @Context HttpHeaders header)
			throws java.text.ParseException, AuthInvalidaExpiradaException {

		CustomResponse response = new CustomResponse();
		String dataAt = info.getQueryParameters().getFirst("dataAt");
		Integer usuarioId = Integer.valueOf(info.getQueryParameters().getFirst("usuarioID"));

		try {
			String token = header.getHeaderString("Authorization");
			Jwts.parser().setSigningKey(new SimpleKeyGenerator().generateKey()).parseClaimsJws(token).getBody();
		} catch (Exception e) {
			response.setCod(401);
			response.setStatus(ResponseStatus.ERROR);
			response.setMessage("Autenticação inválida ou expirada.");
			response.setData(null);
			return response;
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date dtaFormat = sdf.parse(dataAt);

		String dta2 = "'" + sdf.format(dtaFormat) + "'";

		ArrayList<LogsEntradaVO> listaLogEntrada = new ArrayList<LogsEntradaVO>();
		StringBuilder sqlVeiculos = new StringBuilder();
		sqlVeiculos.append("SELECT id "); // 0
		sqlVeiculos.append("      ,placa "); // 1
		sqlVeiculos.append("      ,ano "); // 2
		sqlVeiculos.append("      ,cor "); // 3
		sqlVeiculos.append("      ,condutor_id "); // 4
		sqlVeiculos.append("      ,modelo_id "); // 5
		sqlVeiculos.append("      ,ind_ativo "); // 6
		sqlVeiculos.append("      ,dth_cadastro "); // 7
		sqlVeiculos.append("      ,dth_atualizacao "); // 8
		sqlVeiculos.append("      ,dispositivo_id "); // 9
		sqlVeiculos.append("  FROM veiculos ");
		sqlVeiculos.append(" WHERE usuario_id = " + usuarioId);

		Query queryVeiculos = this.em.createNativeQuery(sqlVeiculos.toString());
		List<?> resultsVeiculos = queryVeiculos.getResultList();

		if (resultsVeiculos != null && !resultsVeiculos.isEmpty()) {

			for (Object rowVeiculos : resultsVeiculos) {
				Object[] rowVeiculosArray = (Object[]) rowVeiculos;

				VeiculosUsuarioVO veiculosUsuario = new VeiculosUsuarioVO();
				veiculosUsuario.setIdWS(Integer.valueOf(rowVeiculosArray[0].toString()));
				veiculosUsuario.setPlaca(rowVeiculosArray[1].toString());
				veiculosUsuario.setAno(Integer.valueOf(rowVeiculosArray[2].toString()));
				veiculosUsuario.setCor(rowVeiculosArray[3].toString());
				veiculosUsuario.setIndAtivo(Integer.valueOf(rowVeiculosArray[6].toString()));
				veiculosUsuario.setDthCadastro(sdf.format(sdf.parse(rowVeiculosArray[7].toString())));
				veiculosUsuario.setDthAtualizacao(sdf.format(sdf.parse(rowVeiculosArray[8].toString())));

				// Dados do condutor
				Integer condutorId = Integer.valueOf(rowVeiculosArray[4].toString());
				Condutores condutor = this.em.find(Condutores.class, condutorId);
				veiculosUsuario.setNomeCondutor(condutor.getNome());

				// Dados do usuário
				Usuarios usuario = this.em.find(Usuarios.class, usuarioId);
				UsuarioVO usuarioVO = new UsuarioVO();
				usuarioVO.setIdWS(usuario.getId());
				usuarioVO.setNome(usuario.getNome());
				usuarioVO.setEmail(usuario.getEmail());
				usuarioVO.setSenha(null);
				usuarioVO.setToken(null);
				veiculosUsuario.setUsuario(usuarioVO);

				// Dados do modelo do veículo
				Integer modeloId = Integer.valueOf(rowVeiculosArray[5].toString());
				ModelosVeiculos modeloVeiculo = this.em.find(ModelosVeiculos.class, modeloId);

				ModelosVeiculosVO modelosVeiculosVO = new ModelosVeiculosVO();
				modelosVeiculosVO.setIdWS(modeloVeiculo.getId());
				modelosVeiculosVO.setDescricao(modeloVeiculo.getDescricao());

				// Busca a marca do modelo
				Integer marcaId = modeloVeiculo.getMarcasVeiculosId();
				MarcasVeiculos marcaVeiculo = this.em.find(MarcasVeiculos.class, marcaId);

				MarcasVeiculosVO marcaVeiculoVO = new MarcasVeiculosVO();
				marcaVeiculoVO.setIdWS(marcaVeiculo.getId());
				marcaVeiculoVO.setDescricao(marcaVeiculo.getDescricao());

				// Busca tipo do veículo
				Integer tipoVeiculoId = marcaVeiculo.getTipoVeiculoId();
				TiposVeiculos tipoVeiculo = this.em.find(TiposVeiculos.class, tipoVeiculoId);

				TiposVeiculosVO tipoVeiculoVO = new TiposVeiculosVO();
				tipoVeiculoVO.setIdWS(tipoVeiculo.getId());
				tipoVeiculoVO.setDescricao(tipoVeiculo.getDescricao());

				marcaVeiculoVO.setTipoVeiculo(tipoVeiculoVO);
				modelosVeiculosVO.setMarcaVeiculo(marcaVeiculoVO);
				veiculosUsuario.setModeloVeiculo(modelosVeiculosVO);

				//				// Busca dados de localização
				//				StringBuilder sqlLogEntradas = new StringBuilder();
				//				sqlLogEntradas.append("SELECT le.id "); // 0
				//				sqlLogEntradas.append("     , le.latitude "); // 1
				//				sqlLogEntradas.append("     , le.longitude "); // 2
				//				sqlLogEntradas.append("     , le.dth_localizacao "); // 3
				//				sqlLogEntradas.append("     , le.velocidade "); // 4
				//				sqlLogEntradas.append("     , le.direcao "); // 5
				//				sqlLogEntradas.append("  FROM log_entradas le ");
				//				sqlLogEntradas.append(" WHERE EXISTS (SELECT 1");
				//				sqlLogEntradas.append("                 FROM dispositivos disp ");
				//				sqlLogEntradas.append("                WHERE disp.imei = le.imei ");
				//				sqlLogEntradas.append("                  AND disp.id = " + Integer.valueOf(rowVeiculosArray[9].toString()) + ") ");
				//				sqlLogEntradas.append("    AND le.dth_atualizacao >= " + dta2);
				//				sqlLogEntradas.append("  ORDER BY le.id DESC ");

				// Busca IMEI do dispositivo
				Dispositivos dispositivo = this.em.find(Dispositivos.class, Integer.valueOf(rowVeiculosArray[9].toString()));

				/*
				 * SELECT * FROM log_entradas WHERE id in(SELECT MAX(log.id) FROM
				 * log_entradas
				 * log WHERE log.imei = 865205033820621 AND log.dth_localizacao
				 * BETWEEN
				 * '2019-07-24 17:55:00' AND '2019-07-24 23:43:23' GROUP BY
				 * log.latitude ,
				 * log.longitude) ORDER BY dth_localizacao;
				 */
				// Busca dados de localização
				StringBuilder sqlLogEntradas = new StringBuilder();
				sqlLogEntradas.append("SELECT le.id "); // 0
				sqlLogEntradas.append("     , le.latitude "); // 1
				sqlLogEntradas.append("     , le.longitude "); // 2
				sqlLogEntradas.append("     , le.dth_localizacao "); // 3
				sqlLogEntradas.append("     , le.velocidade "); // 4
				sqlLogEntradas.append("     , le.direcao "); // 5
				sqlLogEntradas.append("  FROM log_entradas le ");
				sqlLogEntradas.append(" WHERE id IN (SELECT MAX(log.id)");
				sqlLogEntradas.append("                 FROM log_entradas log ");
				sqlLogEntradas.append("                WHERE log.imei = " + dispositivo.getImei());
				sqlLogEntradas.append("                  AND log.dth_localizacao >= " + dta2);
				sqlLogEntradas.append("                GROUP BY log.latitude ");
				sqlLogEntradas.append("                       , log.longitude) ");
				sqlLogEntradas.append("  ORDER BY le.dth_localizacao ");

				Query queryLogEntradas = this.em.createNativeQuery(sqlLogEntradas.toString());
				List<?> resultsLogEntradas = queryLogEntradas.getResultList();

				if (resultsLogEntradas != null && !resultsLogEntradas.isEmpty()) {

					for (Object rowLogEntradas : resultsLogEntradas) {
						Object[] rowLogEntradasArray = (Object[]) rowLogEntradas;

						LogsEntradaVO logsEntradaVO = new LogsEntradaVO();

						logsEntradaVO.setIdWS(Integer.valueOf(rowLogEntradasArray[0].toString()));
						logsEntradaVO.setLatitude(Double.valueOf(rowLogEntradasArray[1].toString()));
						logsEntradaVO.setLongitude(Double.valueOf(rowLogEntradasArray[2].toString()));
						logsEntradaVO.setDthLocalizacao(sdf.format(sdf.parse(rowLogEntradasArray[3].toString())));
						logsEntradaVO.setVelocidade(Integer.valueOf(rowLogEntradasArray[4].toString()));
						logsEntradaVO.setDirecao(Integer.valueOf(rowLogEntradasArray[5].toString()));
						logsEntradaVO.setVeiculo(veiculosUsuario);
						listaLogEntrada.add(logsEntradaVO);
					}
				} else {
					response.setCod(200);
					response.setStatus(ResponseStatus.SUCCESS);
					response.setMessage("Nenhum dado de localização encontrado");
					response.setData(listaLogEntrada);
					return response;
				}
			}
			response.setCod(200);
			response.setStatus(ResponseStatus.SUCCESS);
			response.setMessage("Consulta efetuada com sucesso");
			response.setData(listaLogEntrada);
		} else {
			response.setCod(200);
			response.setStatus(ResponseStatus.ERROR);
			response.setMessage("Dados do usuário não encontrado");
			response.setData(listaLogEntrada);
		}
		return response;
	}
}
