package ejb;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import exception.AuthInvalidaExpiradaException;
import exception.UsuarioBloqueadoException;
import exception.UsuarioSenhaIncorretosException;

@Provider
public class ExceptionHandler implements ExceptionMapper<Exception> {

	@Override
	public Response toResponse(Exception exception) {
		Object retorno = exception;

		if (exception instanceof UsuarioSenhaIncorretosException) {
			retorno = "Usuário ou senha incorretos.";
			return Response.status(Status.UNAUTHORIZED).entity(retorno).build();
		} else if (exception instanceof AuthInvalidaExpiradaException) {
			retorno = "Autenticação inválida ou expirada.";
			return Response.status(Status.UNAUTHORIZED).entity(retorno).build();
		} else if (exception instanceof UsuarioBloqueadoException) {
			retorno = "Usuário bloqueado na aplicação.";
			return Response.status(Status.UNAUTHORIZED).entity(retorno).build();
		} else {
			exception.printStackTrace();
		}

		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(retorno).build();
	}

}
